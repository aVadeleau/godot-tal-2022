extends RigidBody

export var multiplied = false


#func ready():
#	pass

func _integrate_forces(_state):
	randomize()
	var x = rand_range(-29.0, 29.0) # -29 up to 29
	var y = rand_range(-58, 58.0) # 0 up to 58
	var z = rand_range(-29.0, 29.0) # -29 up to 29

	var force = Vector3(x, y, z)
	add_central_force(force.normalized() * 100)

func _on_Virus_body_entered(body):
	if is_inside_tree():
		var virus = get_tree().get_nodes_in_group("Virus")
		#var parent = self.get_parent()
		 
		if (body.is_in_group("Anticorps")):
			queue_free()
		elif body.is_in_group("Virus"):
			var instance = load("res://scene/Virus.tscn").instance()
			if (self.get_instance_id() > body.get_instance_id()) && (!multiplied) && (virus.size() < 50):
				multiplied = true
				
				#Management of the origin of the new RigidBody
				var transform = self.get_global_transform()
				var x = transform.origin.x
				
				if (x<24):
					transform.origin.x = x + 5
				elif (x == 24):
					transform.origin.x = x - 5	
						
				instance.set_global_transform(transform)
				
				#Setting the parent of the new RigidBody
				get_parent().add_child(instance)
		





