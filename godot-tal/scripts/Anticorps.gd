extends RigidBody

export var multiplied = false


#func ready():
#	pass	

func _integrate_forces(_state):
	randomize()
	var x = rand_range(-29.0, 29.0) # -29 up to 29
	var y = rand_range(-58, 58.0) # 0 up to 58
	var z = rand_range(-29.0, 29.0) # -29 up to 29
	
#	var input_h = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left") 
#	var input_v = Input.get_action_strength("ui_up") - Input.get_action_strength("ui_down")
#
	var force = Vector3(x, y, z)
#	var force = Vector3(input_v, 0, input_h)
	add_central_force(force.normalized() * 100)

func _on_Anticorps_body_entered(body):
	if is_inside_tree():
		var anticorps = get_tree().get_nodes_in_group("Anticorps")
		
		if (body.is_in_group("Virus")):
			queue_free()
		elif body.is_in_group("Anticorps"):
			var instance = load("res://scene/Anticorps.tscn").instance()
			if (self.get_instance_id() > body.get_instance_id()) && (!multiplied) && (anticorps.size() < 50):
				multiplied = true

				#Management of the origin of the new RigidBody
				var transform = self.get_global_transform()
				var x = transform.origin.x

				if (x<24):
					transform.origin.x = x + 5
				elif (x == 24):
					transform.origin.x = x - 5	
				
				instance.set_global_transform(transform)
				
				#Setting the parent of the new RigidBody
				get_parent().add_child(instance)



	
