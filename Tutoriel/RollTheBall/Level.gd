extends Spatial

var score = 0
var score_max

# Called when the node enters the scene tree for the first time.
func _ready():
	var pickups = get_tree().get_nodes_in_group("Pick Up")
	
	for pickup in pickups:
		pickup.connect("captured", self, "on_Pick_Up_captured")
	score_max = pickups.size()

func on_Pick_Up_captured():
	score += 1
	$UI/Label.text = "Score :  %s "% score # %s will be replaced by the value of score
	
	if score_max == score:
		$UI/EndContainer.show()

