# Compte-rendu TAL : Godot engine


## PROGRAMME DE LA SEMAINE 02/05 au 06/05 : 
> * Se renseigner et faire des tutoriels sur Godot engine
> * Réaliser les trois parties du projet + le bonus ( voir framateam pour plus de détail) 

## Lundi 02 Mai :

> Horaire d'arrivée : 10h00

#### Programme du jour : 

> * Installation de Godot engine 
> * Tutoriel sur Godot engine


#### Tâches effectuées : 
> * Installation de Gdot engine
> * Tutoriel sur Godot engine ( en cours)

#### Résumé : 
> Aujourd'hui j'ai installé Godot engine sur windows ( c'est juste un exécutable à téléchargé) j'ai lu la documentation officielle de godot engine (https://docs.godotengine.org/en/stable/getting_started/introduction/index.html) puis commencé un tutoriel que nous avais donnée Mr Ballet, en 3 parties sur une initiation 3D sur youtube( voici le lien pour la 1ère partie : https://www.youtube.com/watch?v=ROhV4MQXHfk ), j'ai finis la 1ère partie qui est très théorique sur godot engine. 

>J'ai commencé la 2ème vidéo qui va traiter de la programmation d'un jeu très simple ( un exemple pris sur unity) qui se nomme *RollTheBall* le but du jeu est de faire rouler un balle sur un terrain plat pour qu'elle y rammase tous les collectibles se trouvait sur celui-ci, une fois que la balle à tout rammasser, le joueur à gagné. J'ai arreté la vidéo à 11:47.  

> Horaire de départ : 17h00
 
#### Programme du lendemain : 
> * Continuer le tutoriel de Godot engine (partie 2)
> * Faire la partie 3 du tutoriel de Godot engine



## Mardi 03 Mai : 

> Horaire d'arrivée : 9h30

#### Programme du jour :
> * Continuer le tutoriel de Godot engine (partie 2)
> * Faire la partie 3 du tutoriel de Godot engine
> * Commencer la 1ère partie du projet ( celle avec les sphères qui se déplacent aléatoirement avec gestion des collision)


#### Tâches effectuées :
> * Tutoriel de godot engine partie 2 
> * Tutoriel de godot engine partie 3 
> * 1ère partie du projet ( sphères qui se déplacent aléatoirement dans une boite)


#### Résumé :
> Aujourd'hui j'ai terminé le tutoriel de godot engine que Mr Ballet nous avais donné, j'ai donc terminé le jeu *RollTheBall*, après cela j'ai décidé de commencé le projet notamment la 1ère partie qui consiste à générer des sphères qui effectuerons des déplacements aléatoires tout en gérant les collisions. J'ai décidé de représenter tous cela en 3D dans une boite composé de 6 murs étant des StaticBody, les sphères, elles sont des RigidBody.
J'ai  eu quelques difficultés à gérer les mouvement aléatoires sur l'axe y dû à la gravité mais j'ai pu le corriger  en ajoutant un PhysicsMaterial avec pour paramètre : 
>*  Friction = 0 
>*  Bounce = 1
J'ai aussi augmenté la range de l'aléatoire sur l'axe y ce qui a grandement aidé.

> Après cela j'ai continué sur la 2ème partie, les collisions typées. Les collision typées sont : 
> * Si un anticorps percute un virus, les 2 disparaissent
> * Si un virus percute un virus, ils se multiplient
Je pense aussi rajouté la même chose pour les anticorps.
Je me suis arrèté à la gestion de ces réactions qui pour le môment ne sont pas fonctionnelles. 

#### Programme du lendemain : 
> * Corriger les collisions
> * Ajouter la dernière collision avec anticorps
> * Avancer avec Gaëtan et Clément la représentation d'une veine

> Horaire de départ : 17h00

## Mercredi 04 Mai :

> Horaire d'arrivée : 9h15

#### Programme du jour : 
> * Corriger les collisions entre RigidBody
> * Ajouter la dernière collision entre anticorps
> * Avancer avec Gaëtan et Clément la représentation d'une veine sous godot 


#### Tâches effectuées : 
> * Corriger les collisions entre RigidBody
> * Rajout d'un UI avec deux label indiquant le nombres d'anticorps et de virus restant

#### Résumé
> J'ai réussi à corrigé les problèmes de collision entre RigidBody que j'avais eu hier c'était simplement parce que j'ai mal traité le problème. Pour que des collisions entre RigidBody soit détectées il faut d'abord mettre le paramètre *contact_monitor* à true ( qui est mis à false par défaut), ce paramètre fait en sorte que quand il est true il envoie un signal quand des RigidBody entre en collision mais uniquement quand un deuxième paramètre *contact_reported* (qui définis le nombre maximum de contact qui sera enregistré) est supérieur à 0. Après avoir définis ces deux paramètre j'ai pu faire en sorte que quand un anticorps touche un virus les deux disparaissent.  
Ensuite, j'ai rajoute une UI avec deux label l'un qui contient le nombre d'anticorps et l'autre le nombre de virus.  
J'ai continué par le deuxième type de collision , faire en sorte que si un virus entre en contact avec un autre virus ( ou un anticorps avec un autre anticorps)ils se dupliquent pour créer un autre virus/anticorps. J'ai eu un problème qui était que les deux anticorps se dupliquaient et donc ne créait donc pas un anticorps  mais deux anticorps, j'ai corrigé le problème en faisant de sorte que seul l'anticorps avec l'ID le plus grand se duplique.
Mais il me reste un autre problème à traiter qui est que le nouvel anticorps se crée à la même position que sont parent ce qui cause des réactions en chaine de duplication d'anticorps ce qui fait crash rapidement godot engine. J'ai essayé de corrigé en modifiant la position de base du nouvel anticorps, ce qui a partiellement fonctionné vu que la duplication en chaine c'est stoppé mais le nouvel anticorps avec des mouvements très étrange et non désirés.

> Horaire de départ : 17h01


#### Programme du lendemain :
> * Corriger le mouvement erratique de l'anticorps dupliqué
> * Appliquer la duplication aux virus une fois la correction aux anticorps effectuée
> * Avancer la représentation d'une veine avec Gaëtan et Clément

## Jeudi 05 Mai

> Horaire d'arrivée : 9h30

#### Programme du jour : 
> * Corriger le mouvement erratique de l'anticorps dupliqué
> * Appliquer la duplication corrigée aux virus 
> * Avancer la répresentation d'une veine avec Gaëtan et Clément


#### Tâches effectuées : 
> * Corriger le mouvement erratique de l'anticorps dupliqué
> * Appliquer la duplication corrgiée aux virus


#### Résumé : 
> Aujourd'hui j'ai pu corrigé le mouvement erratique de l'anticorps dupliqué, c'était dû à une origine mais set-up, origine qui est importante à postionner sinon l'anticorps dupliqué apparait à la position de son père et se duplique à l'infini. J'avais donc modifier sa position mais des mouvements erratique son apparu cela est du à une mauvaise modification de la position, j'ai vu en effectuant des recherches que le mieux est d'utiliser une méthode *set_global_transfrom(Transform value)* qui appartient à la classe *Spatial* en utilisant cette méthode pour modifier la position on doit définir un *Trasnform* qui en prend en paramètre : 
> * Un vecteur pour l'axe x (1, 0, 0)
> * Un vecteur pour l'axe y (0, 1, 0)
> * Un vecteur pour l'axe z (0, 0, 1)
> * Un vecteur pour l'origine (par exemple (-10, 20 ,14))
Le dernier vecteur, le vecteur origine sert à donner la position que l'on veut donner. J'ai décidé pour la position à donner aux nouveaux anticorps/virus dépenderais de leurs père mais en modifiant leurs x de +5 si l'x du père est inférieure 24 ( inférieure à 24 pour éviter qu'un anticorps apparait à un x = à 29 qui est la position d'un mur, ce qui ferait apparaitre l'anticorps/virus dans le mur et aurais par conséquence de le faire ignorer les collisions) et donc si l'x du père est surpérieur à 24 l'x du nouvel anticorps/virus sera reduit de 5. J'ai également appliqué ceci aux virus puis modifier quelques paramètre comme le pourcentage de chance qu'un anticorps/virus se multiplie que j'ai retiré parce que les anticorps/virus apparaissent à 5 cases d'écart ce qui est assez, mais aussi car je leurs est ajouté un paramètre booléen *multiplied* qui est à *false* par défaut qui définis si un virus/anticorps c'est multiplié ou non, une fois qu'il s'est multiplié ce paramètre passe à *true* et l'anticorps/virus en question ne peux plus se multiplié. Pour finir j'ai ajouté une limite maximum de 50 par entité. J'ai eu quelques problèmes à  cause du compteur qui calculait un virus de plus. Cela était justement dû à un mauvais placement d'un virus qui se trouvais dans un mur et donc passait à travers la boite et donc n'était pas supprimé.


> Horaire de départ  : 17h00

#### Programme du lendemain : 
> * Avancer sur la représentation d'un veine 
> * Faire des recherches pour effectuer une autre simulation 



## Vendredi 06 Mai

> Horaire d'arrivée 10h10

#### Programme du jour :
> * Se renseigner sur le Visual Script
> * Faire la même chose que précédemment mais en Visual Script
> * Faire des recherches pour effectuer une autre simulation



#### Tâches effectuées :
> * Se renseigner sur le Visual Script 
> * Faire des recherches pour effetuer une autre simulation

#### Résumé : 
> Aujourd'hui je me suis renseigné sur le fonctionnement de Visual Script, un langaga graphique de Godot engine. J'ai donc lu de la doc sur leur site officiel ( https://docs.godotengine.org/en/stable/tutorials/scripting/visual_script/index.html). Après cela j'ai réfléchis à deux potentielles simulation à effectuer.
La 1ère pourrais être une simulation représentant la trajectoire des globules rouges à travers les veines jusqu'au poumons.
La 2ème serait une reprise du projet que j'ai effectué en Matrix Studio mais sur Godot engine et en 3D.

> Horaire de départ :

#### Programme du lendemain : 
> * Continuer de se documenter sur Visual Script
> * Faire la même chose que précédemment en Visual Script
> * Faire des recherches pour effectuer une autre simulation
> * Essayer d'implémenter les deux idées de simulation


## FIN DE LA SEMAINE DU 02/05 AU 06/05

### Taches de semaine effectuées : 
> * Se renseigner et faire des tutoriels sur Godot engine
> * Réaliser les trois parties du projet + bonus ( il reste uniquement à refaire les scripts en Visual Script à faire)


## PROGRAMME DE LA SEMAINE 02/05 au 06/05 : 
>  ??


## Lundi 09 Mai :

> Horaire d'arrivée : 9h30

#### Programme du jour :
> * Continuer de se documenter sur Visual Script
> * Reproduire le code produit en GD sous Visual Script
> * Faire des recherches pour effectuer une autre simulation
> * Se renseigner sur l'utilisation des KinematicBody
> * Reproduire le projet de Matrix Studio sur Godot engine


#### Tâches effectuées : 


#### Résumé :


#### Programme du lendemain : 


> Horaire du départ : 

