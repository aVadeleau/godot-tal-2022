extends RigidBody

export(float, 1, 20) var speed = 5

func _integrate_forces(state):
	var input_h = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left") # For the horizontal movement
	var input_v = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up") # For the vertical movement
	

	# Get the movement of the ball
	var force = Vector3(input_h, 0, input_v)
	
	add_central_force(force.normalized() * speed) # ".normalized" makes the length of the vector to 1 
	
	
