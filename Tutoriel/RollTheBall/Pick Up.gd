extends Area

signal captured


# Method that will make the collectibles spin on itself 
func _physics_process(delta):
	rotate(Vector3(45, 30, 15).normalized(), delta)

# Method that will handle the actions when a "thing" enter in the pick up
func _on_Pick_Up_body_entered(body):
	queue_free()
	emit_signal("captured")
	
