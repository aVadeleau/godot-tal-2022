extends Spatial

var anticorps
var anticorps_max

var virus
var virus_max


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass
#
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	virus = get_tree().get_nodes_in_group("Virus")
	virus_max = virus.size()
	
	anticorps = get_tree().get_nodes_in_group("Anticorps")
	anticorps_max = anticorps.size()
	

	$UI/Anticorps.text = "Anticorps : %s"%anticorps_max
	$UI/Virus.text = "Virus : %s"%virus_max


