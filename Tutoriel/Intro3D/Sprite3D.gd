extends Sprite3D


func _process(delta):
	var input_h = Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right") 
	var input_v = Input.get_action_strength("ui_up") - Input.get_action_strength("ui_down") 

	# Déplacer sur l'axe des x, y 

	#transform.origin.x += input_h * delta * 5
	#transform.origin.y += input_v * delta * 5

	#translate(Vector3(input_h, input_v, 0) * delta * 5)
	
	# Rotater sur l'axe des x, y 
	
	#transform.basis = transform.basis.rotated(transform.basis.x, input_v * delta * 5)
	#transform.basis = transform.basis.rotated(transform.basis.y, input_h * delta * 5)

	#rotate_object_local(Vector3.RIGHT, input_v * delta * 5)
	#rotate_object_local(Vector3.UP, input_h * delta * 5)
	

	# Redimensionner sur les axes
	
	#transform.basis = transform.basis.scaled(Vector3.ONE * (1 + delta * 5 * input_v))
	
	scale_object_local(Vector3(1 + input_h * delta * 5, 1 + input_v * delta * 5, 1))
	
